import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    sites: null,
    lastPulled: null
  },
  getters:{
    getSites: (state) => { return state.sites; },
    getLastPulledTimeStamp: (state) => { 
      if(state.lastPulled == null) return 'N/A';
      return state.lastPulled.toLocaleString(); 
    },
    getFilteredSites: (state) => (searchTerm) => {
      var query = searchTerm.trim().toLowerCase();
      if(query == 0) return state.sites;
      return state.sites.filter(item =>{
          var siteNumber = item.SiteNumber.trim().toLowerCase();
          var siteName = item.SiteName.trim().toLowerCase();
          if(siteNumber.includes(query)) return item;
          if(siteName.includes(query)) return item;
      });
    }
  },
  mutations: {
    setSites(state, sites){
      state.sites = sites.map(obj =>{
        var ret = {};
        ret.SiteNumber = obj.SiteNumber;
        ret.SiteName = obj.SiteName;
        ret.LastUpdated = obj.Images[0].Captured;
        ret.Latitude = obj.Latitude;
        ret.Longitude = obj.Longitude;
        ret.Images = obj.Images;
        return ret;
      });
      state.lastPulled = new Date();
    }
  },
  actions: {
    refreshSites({commit}){
      console.log("Sites Refreshed");
      axios.get('https://txdata.usgs.gov/ASI/service/odata/Sites').then(function(result){
        commit('setSites', result.data.value);
      });
    }
  }
})
