import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Sites from './views/Sites.vue'
import Site from './views/Site.vue'
import Map from './views/Map.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/sites/:id',
      name: 'site',
      component: Site
    },
    {
      path: '/sites',
      name: 'sites',
      component: Sites
    },
    {
      path: '/map',
      name: 'map',
      component: Map
    }
  ],
  linkActiveClass: 'active'
})
